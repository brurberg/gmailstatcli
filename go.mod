module gitlab.com/brurberg/gmailstatcli

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	google.golang.org/api v0.20.0
)
