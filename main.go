package main

import (
	"fmt"
	"log"

	"gitlab.com/brurberg/gmailstatcli/util"
)

func main() {
	antMail, err := util.GetAntMail()
	if err != nil {
		log.Fatalf("%v\n", err)
	}
	fmt.Println(antMail)
}
